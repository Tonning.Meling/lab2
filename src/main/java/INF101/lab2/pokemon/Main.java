package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon ("Fjompenisse", 50, 5);
    public static Pokemon pokemon2 = new Pokemon ("Fjert-ulf", 60, 10);
    public static void main(String[] args) {
        // Opprett to Pokémon og la dem kjempe
        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println("");
    
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (!pokemon2.isAlive()) {
                System.out.println(pokemon2 + " is defeated by " + pokemon1);
                break;
            }
    
            pokemon2.attack(pokemon1);
            if (!pokemon1.isAlive()) {
                System.out.println(pokemon1 + " is defeated by " + pokemon2);
            }
        }
    }
}